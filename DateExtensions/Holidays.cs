﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DateExtensions
{
    public class Holidays
    {
        /// <summary> 
        /// Método para Calculo da Pascoa,Corpus Christ e Carnaval By: Flinox 
        /// </summary> 
        /// <param name="Ano"></param> 
        /// <returns>Array Data[]</returns> 
        public DateTime[] FeriadosMoveis(string Ano)
        {
            DateTime[] Data = new DateTime[3];
            int ano = Convert.ToInt32(Ano.Substring(0, 4));
            int x, y;
            int a, b, c, d, e;
            int dia, mes;

            DateTime pascoa, carnaval, corpus;

            if (ano >= 1900 & ano <= 2099)
            {
                x = 24;
                y = 5;
            }
            else
                if (ano >= 2100 & ano <= 2199)
                {
                    x = 24;
                    y = 6;
                }
                else
                    if (ano >= 2200 & ano <= 2299)
                    {
                        x = 25;
                        y = 7;
                    }
                    else
                    {
                        x = 24;
                        y = 5;
                    }

            a = ano % 19;
            b = ano % 4;
            c = ano % 7;
            d = (19 * a + x) % 30;
            e = (2 * b + 4 * c + 6 * d + y) % 7;

            if ((d + e) > 9)
            {
                dia = (d + e - 9);
                mes = 4;
            }
            else
            {
                dia = (d + e + 22);
                mes = 3;
            }

            // PASCOA 
            pascoa = Convert.ToDateTime((Convert.ToString(dia) + "/" + Convert.ToString(mes) + "/" + ano));
            Data[0] = pascoa;

            // CARNAVAL ( PASCOA - 47 dias ) 
            carnaval = pascoa.AddDays(-47);
            Data[1] = carnaval;

            // CORPUS CHRISTI ( PASCOA + 60 dias ) 
            corpus = pascoa.AddDays(60);
            Data[2] = corpus;

            // Feriados adicionais aqui

            return Data;
        } 
    }
}
