﻿using System;

namespace DateExtensions
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime hoje = DateTime.Now;

            Console.WriteLine("Segunda feira: " + DateExtensions.FirstWorkingDayofWeek(hoje).ToString());
            Console.WriteLine("Sexta feira: " + DateExtensions.LastWorkingDayofWeek(hoje).ToString());
            Console.WriteLine("Primeiro dia do mês: " + DateExtensions.FirstDayofMonth(hoje).ToString());
            Console.WriteLine("Segundo dia do mês: " + DateExtensions.LastDayofMonth(hoje).ToString());
            Console.ReadKey();
        }
    }
}
