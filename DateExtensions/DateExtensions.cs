﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateExtensions
{
    public class DateExtensions
    {
        public static Boolean IsHoliday(DateTime data)
        {
            Holidays oferiado = new Holidays();
            DateTime[] feriadosMoveis = oferiado.FeriadosMoveis(data.Year.ToString());

            foreach (var item in feriadosMoveis)
            {
                if (item == data)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Boolean IsWeekend(DateTime data)
        {
            if ((int)data.DayOfWeek == 0 || (int)data.DayOfWeek == 1)
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime WeekendToWorkingDay(DateTime data)
        {
            //tentativa de encaixar as datas dentro da semana
            if ((int)data.DayOfWeek == 0) //domingo
                data.AddDays(1); //vai para a segunda
            else if ((int)data.DayOfWeek == 6) //sabado
                data.AddDays(-1); //vai para a sexta
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime HolidayToWorkingDay(DateTime data)
        {
            //tentativa de encaixar a data dentro da semana
            if ((int)data.DayOfWeek == 1) //segunda
                data.AddDays(1); //vai para a terça
            else if ((int)data.DayOfWeek == 2) //terça
                data.AddDays(1); //vai para a quarta
            else if ((int)data.DayOfWeek == 3) //quarta
                data.AddDays(1); //vai para a quinta
            else if ((int)data.DayOfWeek == 4) //quinta
                data.AddDays(-1); //vai para a quarta
            else if ((int)data.DayOfWeek == 5) //sexta
                data.AddDays(-1); //vai para a quinta

            return data;
        }

        /// <summary>
        /// http://jucineisantos.com/2014/03/28/getting-the-first-and-last-day-in-a-week-with-csharp/
        /// </summary>
        /// <param name="data"></param>
        public static DateTime FirstWorkingDayofWeek(DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;
            data = new DateTime(ano, mes, dia);

            //Variáveis de controle dos dias.
            int numeroMenor = 1;
            return data.AddDays(numeroMenor - data.DayOfWeek.GetHashCode());
        }

        /// <summary>
        /// http://jucineisantos.com/2014/03/28/getting-the-first-and-last-day-in-a-week-with-csharp/
        /// </summary>
        /// <param name="data"></param>
        public static DateTime LastWorkingDayofWeek(DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;
            data = new DateTime(ano, mes, dia);

            //Variáveis de controle dos dias.
            int numeroMaior = 5;
            return data.AddDays(numeroMaior - data.DayOfWeek.GetHashCode());
        }

        /// <summary>
        /// https://jcecilio.wordpress.com/2011/06/09/primeiro-e-ultimo-dia-do-mes-c/
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime FirstDayofMonth(DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;
            data = new DateTime(ano, mes, 1); 
            return data;
        }

        /// <summary>
        /// https://jcecilio.wordpress.com/2011/06/09/primeiro-e-ultimo-dia-do-mes-c/
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime LastDayofMonth(DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;
            int dia = DateTime.DaysInMonth(ano,mes);
            data = new DateTime(ano, mes, dia);
            return data;
        }

    }
}
